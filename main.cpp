#include <iostream>
#include <windows.h>

using namespace std;
// Algoritmos de ordenacion 2
// autor: Adan jaciel palacios rivas
void menu(){
    cout<< "\t------------ Selecciona una opcion del menu ------------\n\n";
    cout<< "1- Ordenamiento Quick Sort"<<endl;
    cout<< "2- Ordenamiento Shell Sort"<<endl;
    cout<< "3- Salir"<<endl;
    cout<<"\n\n Seleccione una opcion: ";
}

void imprimir(int a[],int n){
    for(int i = 0; i < n; i++){
        cout << a[i] << " ";
    }
}

void imprimir_shell(int a[],int n){
    cout<<"Numeros del arreglo ordenados de menor a mayor"<<endl;
    for(int i = 0; i < n; i++){
        cout<<"[ "<<a[i]<<" ]";
    }
}

void Quicksort(int* arr, int izq, int der)
{
     int i = izq, j = der, tmp;
     int p = arr[(izq + der) / 2];

     while (i <= j)
     {
          while (arr[i] < p) i++;
          while (arr[j] > p) j--;
          if (i <= j)
           {
               tmp = arr[i];
               arr[i] = arr[j];
               arr[j] = tmp;
               i++; j--;
          }
     }

     if (izq < j)
          Quicksort(arr, izq, j);
     if (i < der)
          Quicksort(arr, i, der);
}

void shell(int a[], int n){
    int ints,i,aux;
    bool band;

    ints =n;
    while(ints>1){

        ints = (ints/2);
        band =true;

        while(band==true){
            band =false;
            i=0;
            while((i+ints)<=n){
                if(a[i]>a[i+ints]){
                    aux = a[i];
                    a[i] = a[i+ints];
                    a[i+ints] =  aux;
                    band = true;
                }
                i++;
            }
        }

    }
}


int main()
{
    int opcion = 0, total,tam;
    int arreglo[tam];

    cout << "\t\tAlgoritmos de ordenamiento Quick Sort y Shell Sort\n\n" << endl;
    do{

    menu();
    cin>>opcion;

        switch(opcion){
        case 1:

            system("cls");
            cout<<"Ordenamiento Quicksort\n\n";
            cout<<"Digita el tama�o que quieres que tenga el arreglo: "<<endl;
            cin>>tam;

            for(int i = 0; i < tam; i++){
                cout<<"Ingrese los numeros  [ "<<(i+1)<<" ] del arreglo"<<endl;
                cin>>arreglo[i];
                }
           /* cout<< "Arreglo desordenado\n\n";
            cout<< "88, 6, 69, -33, 98, 7, 23, 5, 0, 100\n\n";*/
            cout<< "Arreglo ordenado:\n\n";
            Quicksort(arreglo,0,9);
            imprimir(arreglo,tam);
            cout<<endl;
            system("pause");
            system("cls");
            break;
        case 2:

            cout<<"Ordenamiento shell Sort\n\n";
            cout<<"Digita el tama�o que quieres que tenga el arreglo: "<<endl;
            cin>>total;
            int num[total];
            for(int i = 0; i < total; i++){
                cout<<"Ingrese los numeros  [ "<<(i+1)<<" ] del arreglo"<<endl;
                cin>>num[i];
                }
            shell(num,total);
            imprimir_shell(num,total);
            system("pause");
            system("cls");
            break;
        case 3:
            cout<<"Salir";
            break;
        default:
            cout<<"Default";
            break;

        }

    }while(opcion != 3);

    return 0;
}
